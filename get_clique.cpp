

// g++ -std=c++11 get_clique.cpp -o get_clique

#include <iostream>
#include <fstream>
#include <stack>
#include <string>
#include <algorithm>
#include <random>
#include <chrono>
#include <vector>
#include <map>

using namespace std;
 
class Graph {
    int g_size;    // No. of vertices
    vector<int> *adj;    // An array of adjacency lists
    vector<int> test_clique(vector<pair<int, int>> sorted_node_edges, int node_index, vector<int> clique_list);

    
public:
    Graph(int g_size);
    void add_edge(int node_a, int node_b);
 
    // get a big clique with an heuristique
    vector<int> get_clique_from_graph();
};
 
Graph::Graph(int g_size) {
    this->g_size = g_size;
    adj = new vector<int>[g_size];
}
 

void Graph::add_edge(int node_a, int node_b) {
    adj[node_a].push_back(node_b); // Add w to v’s list.
}

//sort the vector by edge number

	
vector<int> Graph::get_clique_from_graph() {
	// heuristique here (maximum clique is NP-complete)
	// sort nodes by number of edges
	// start a clique from the node with most edges
	// loop over next nodes with the most edges
	// add to the clique if it is linked to all members 

	
	vector<pair<int, int>> node_edges; //number of edges for each node
	
	// fill the vector
	for (int node = 0; node < g_size; node++) { 
		node_edges.push_back(make_pair(node, adj[node].size()));
	}
	
	// sort by number of edges
	sort(node_edges.begin(), node_edges.end(), 
			[](const pair<int,int> &a, const pair<int,int> &b) { // lambda function to sort vector pair by 2nd element descending
		 		return (a.second > b.second);
			}); 
	
	
	vector<int> clique_list;
	// fill the clique
    clique_list = test_clique(node_edges, 0, clique_list);
    
    return clique_list;
}

	
vector<int> Graph::test_clique(vector<pair<int, int>> sorted_node_edges, int node_index, vector<int> clique_list) {
	// test if a node can be added to the clique list
	// continue the test to the next node with the updated clique list
	
	if (node_index > sorted_node_edges.size()) {
		return clique_list;
	}
	
	int tested_node = sorted_node_edges[node_index].first;
	
	for (int i = 0; i < clique_list.size(); i++) {
		vector<int> neigboors_array = adj[clique_list[i]]; // list of neigboors of this node
		
		if (find(begin(neigboors_array), end(neigboors_array), tested_node) == end(neigboors_array)) { // tested node is not linked to a node from the clique
			return test_clique(sorted_node_edges, node_index+1, clique_list); // cannot be added to the clique -> test the next node
		}
	}
	// the tested node is linked to every node in the clique
	clique_list.push_back(tested_node); // add to the clique
	return test_clique(sorted_node_edges, node_index+1, clique_list); // test the next node
}


string rev_comp(string sequence) {
	reverse(sequence.begin(), sequence.end());
    for (int i = 0; i < sequence.length(); ++i) {
        switch (sequence[i]) {
        case 'A':
        	sequence[i] = 'T';
            break;    
        case 'C':
        	sequence[i] = 'G';
            break;
        case 'G':
        	sequence[i] = 'C';
            break;
        case 'T':
        	sequence[i] = 'A';
            break;
        }
    }
    return sequence;
}

bool test_hybridation(string primer_a, string primer_b) {
	// true if the 2 primers do not have any hybridation >=4
	
	int max_hybridisation_value = 6; // maximum autorized number of consecutive common bases
	int primer_size = 20;
	string rev_primer_a = rev_comp(primer_a);
	
	for (int i = 0; i <= primer_size-max_hybridisation_value-1; ++i) {
		string kmer_a = primer_a.substr(i, max_hybridisation_value+1);
		
		if (primer_b.find(kmer_a) != string::npos) {
			//cout << primer_b << " " << kmer_a << endl;
			return false;
		}
		string kmer_rev_a = rev_primer_a.substr(i, max_hybridisation_value+1);
		
		if (primer_b.find(kmer_rev_a) != string::npos) {
			//cout << primer_b << " " << kmer_rev_a << endl;
			return false;
		}
	}
	return true;
}


Graph compute_hybridation(vector<string> primers_list) {
	// generate a graph of primers intercompatiblity
	// too long for >20k primers
	int primers_n = primers_list.size();
	
	Graph g(primers_n);
	
	for (int i = 0; i < primers_n; ++i) {
		string primer_a = primers_list[i];
		
		for (int j = i+1; j < primers_n; ++j) {
			string primer_b = primers_list[j];
			//cout << "a";
			if (test_hybridation(primer_a, primer_b)) {
				g.add_edge(i,j);
				g.add_edge(j,i);
			}
		}
	}
	return g;
}


vector<string> get_clique(vector<string> primers_list) {
	// since it is too long to generate the graph of intercompatibility between primers
	// start from a random primer and add the others to the clique by testing hybridation
	
	vector<string> clique_list;
	bool compatible;
	for(const string& tested_primer : primers_list) {
		compatible = true;
		for(const string& clique_primer : clique_list) {

			if (test_hybridation(tested_primer, clique_primer) == false) { // tested node is not linked to a node from the clique
				compatible = false;
				break; // cannot be added to the clique -> test the next node
			}
		}
		// the tested node is linked to every node in the clique
		if (compatible) {
			clique_list.push_back(tested_primer); // add to the clique
		}
	}

	return clique_list; 
	
}

int main(int argc,char* argv[]) {
	//args = primers_list.txt clique_size
	if(argc != 4) {
		printf("usage : get_clique primers_list.txt output_path.txt clique_size\n");
		return 1;
	}
	printf("get clique of compatible primers...\n");
	string primers_path = argv[1]; //txt file of the primers //"primer_generator/temp/checked_primers.txt"
	string output_path = argv[2];
	int clique_size = stoi(argv[3]); // size of the clique to find

	// read list of primers
	vector<string> primers_list;
	
	ifstream input_file;
	input_file.open(primers_path);
	string myline;
	if ( input_file.is_open() ) {
		while ( input_file ) {
			getline (input_file, myline);
			if (myline.size() > 0) // last line is empty
				primers_list.push_back(myline);
		}
	} else {
		cout << "Couldn't open file\n";
	}

	vector<string> clique_list;

	for( int i = 1; i <= 10; i++) {
		// get a time-based seed
	    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	    // random shuffle of the primers list
		std::shuffle(std::begin(primers_list), std::end(primers_list), std::default_random_engine(seed));
		// generate a clique
		clique_list = get_clique(primers_list);
		cout << "get clique : " << i << "/10; found clique of " << clique_list.size() << " primers" << endl;
		
		if (clique_list.size() >= clique_size) {
			// save the resulting clique
			ofstream output_file;
			output_file.open (output_path);
			for(const string& primer : clique_list) {
				output_file << primer << "\n";
		    }
			output_file.close();
			// successfull end
			return 0;
		}
	}	
	
	cout << "get clique : couldn't find a clique of the required size" << endl;
	
	// failed end
    return 1;
}

