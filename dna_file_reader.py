#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 09:26:22 2020

@author: oboulle
"""
import os
import sys


# used for the reverse complement
RC_TAB = str.maketrans("ACTG", "TGAC")


def read_fasta(fasta_file_path: str) -> dict:
    """
    :param fasta_file_path: path to the .fasta file
    :return: a dictionary containing the name and the content of the sequences in the file
    """
    sequence_dict = {}
    if not os.path.isfile(fasta_file_path):
        print("read fasta : file not found ("+fasta_file_path+")")
        return sequence_dict
    
    fasta_file = open(fasta_file_path)
    line = fasta_file.readline()
    while line != "":
        if line.startswith(">"):
            sequence_name = line[1:].replace("\n", "")
            sequence = ""
            line = fasta_file.readline()
            while not line.startswith(">") and line != "":
                sequence += line.replace("\n", "")
                line = fasta_file.readline()
            
            while sequence_name in sequence_dict: # if the sequence name is already in the dict
                if sequence_dict[sequence_name] != sequence: # and with a different associated sequence
                    sequence_name += "+" # change the name a bit to not overwrite the other one
            sequence_dict[sequence_name] = sequence
            
        else:
            print("fasta format error in file",fasta_file_path,":",line)
            exit(1)
    fasta_file.close()
    return sequence_dict


def read_single_sequence_fasta(fasta_file_path: str) -> (str, str):
    """
    :param fasta_file_path: path to the .fasta file
    :return: the name and the value of the sequence in the file
    """
    if not os.path.isfile(fasta_file_path):
        print("read single sequence fasta : file not found ("+fasta_file_path+")")
        return None, None
    
    fasta_file = open(fasta_file_path)
    line = fasta_file.readline()
    if line.startswith(">"):
        sequence_name = line[1:].replace("\n", "")
        sequence = ""
        line = fasta_file.readline()
        while not line.startswith(">") and line != "":
            sequence += line.replace("\n", "")
            line = fasta_file.readline()
    else:
        print("error, could not read the sequence from the file :",fasta_file_path)
        sequence_name, sequence = None, None
    fasta_file.close()
    return sequence_name, sequence
    
    
def read_fastq(fastq_file_path: str, use_score=False) -> dict:
    """
    :param fastq_file_path: path to the .fastq file
    :return: a dictionary containing the name and the content of the sequences in the file
    ignore the description and score by default
    """
    sequence_dict = {}
    if not os.path.isfile(fastq_file_path):
        print("read fastq : file not found ("+fastq_file_path+")")
        return sequence_dict

    fastq_file = open(fastq_file_path)
    line = fastq_file.readline()
    while line != "":
        if line.startswith("@"):
            sequence_name = line[1:].replace("\n", "") # remove the starting @
            line = fastq_file.readline()
            sequence = line.replace("\n", "")
            line = fastq_file.readline()
            seq_description = line.replace("\n", "")
            line = fastq_file.readline()
            seq_score = line.replace("\n", "")
            line = fastq_file.readline()

            while sequence_name in sequence_dict: # if the sequence name is already in the dict
                if sequence_dict[sequence_name] != sequence: # and with a different associated sequence
                    sequence_name += "+" # change the name a bit to not overwrite the other one
            
            if use_score:
                sequence_dict[sequence_name] = [sequence, seq_description, seq_score]
            else:
                sequence_dict[sequence_name] = sequence
        else:
            print("fastq format error in file",fastq_file_path,":",line)
            exit(1)
    fastq_file.close()
    return sequence_dict


def complement(sequence: str) -> str:
    """
    return the complement of a dna sequence (AGTT -> TCAA)
    :param sequence: the input sequence
    :return: the complement of the input sequence
    """
    return sequence.translate(RC_TAB)


def reverse_complement(sequence: str) -> str:
    """
    return the reverse complement of a dna sequence (AGTT -> AACT)
    :param sequence: the input sequence
    :return: the reverse complement of the input sequence
    """
    return sequence.translate(RC_TAB)[::-1]


def fasta_to_fastq(fasta_file_path: str, output_path: str) -> None:
    """
    convert a fasta file into a fastq file
    :param fasta_file_path: path of the .fasta file
    :param output_path: path of the .fastq output
    """
    sequences = read_fasta(fasta_file_path)
    file_output = open(output_path, "w+")
    for name, value in sequences.items():
        file_output.write("@"+name+"\n")
        file_output.write(value+"\n")
        file_output.write("+\n")
        file_output.write("---\n")
    file_output.close()
    

def fastq_to_fasta(fastq_file_path: str, output_path: str) -> None:
    """
    convert a fastq file into a fasta file
    :param fastq_file_path: path of the .fastq file
    :param output_path: path of the .fasta output
    """
    sequences_dict = read_fastq(fastq_file_path)
    save_dict_to_fasta(sequences_dict, output_path)

    # fast way in shell, 2 steps :
    # awk 'NR%4==1 || NR%4==2' input.fastq > output.fasta # remove 3rd and 4th lines
    # sed -i 's/@/>/g' output.fasta # replace the @ by >

def save_sequence_to_fasta(sequence_name: str, sequence: str, output_path: str) -> None:
    """
    save a single sequence in a file with the .fasta format
    """
    file_output = open(output_path, "w")
    file_output.write(">"+sequence_name+"\n")
    file_output.write(sequence+"\n")
    file_output.close()
    

def save_dict_to_fasta(sequences_dict: dict, output_path: str) -> None:
    """
    save all sequences from a dict in a file with the .fasta format
    """
    if sequences_dict is None:
        return
    
    file_output = open(output_path, "w+")
    for name, sequence in sequences_dict.items():
        file_output.write(">" + str(name) + "\n")
        file_output.write(sequence + "\n")
    file_output.close()
    

def save_dict_to_fastq(sequences_dict: dict, output_path: str, score=False) -> None:
    """
    save all sequences from a dict in a file with the .fastq format
    """
    if sequences_dict is None:
        return
    
    file_output = open(output_path, "w+")
    if score:
        for name, [sequence, description, score] in sequences_dict.items():
            file_output.write("@" + str(name) + "\n")
            file_output.write(sequence + "\n")
            file_output.write(description + "\n")
            file_output.write(score + "\n")
    else:
        for name, sequence in sequences_dict.items():
            file_output.write("@" + str(name) + "\n")
            file_output.write(sequence + "\n")
            file_output.write("+\n")
            file_output.write("---\n")
    file_output.close()


# =================== main ======================= #
if __name__ == '__main__':
    input_path = sys.argv[1]  # file to read the sequences
    output_path = sys.argv[2]  # file to save the sequences
    fastq_to_fasta(input_path, output_path)

