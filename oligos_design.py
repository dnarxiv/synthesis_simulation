#!/usr/bin/python3

import argparse
import os
import sys
import inspect
import subprocess

import primers_generation as pg
import dna_file_reader as dfr


OVERHANG_LENGTH = 4 # CONSTANT for length of overhang # also for expected length in dna_read
PRIMER_LENGTH = 20 # CONSTANT for length of primers
OLIGO_PER_BLOCK = 10 # number of oligo in each block
bsaI_sequence = "GGTCTCG" # sequence used to generate single strand overhang by digestion; must be absent from payload

"""
design of fragments assembly with overhangs and primers :
assembly of 20 single strand oligos (60b) to create a double strand block of 600
assembly of 10 blocks to create a group
assembly of groups for the complete document

Block :
    START : start primer (20 bases)
    STOP : stop primer (20 bases)
    Ox : overhang (4 bases)
    _ : payload
    
        START______Oa     _________Ob  ...     _________Oj     ______STOP    (forward fragments)
        START______     Oa_________    ...   Oi_________     Oj______STOP    (reverse fragments)
    
    fragments are assembled by couple forward/reverse, then linked in correct order with the overhangs
    
    the start primer is placed at the beginning of first fragment
    the stop primer is placed at the beginning of the reverse complement of the last fragment
"""


def sequence_fragmentation(sequence_path: str, fragment_length: int) -> dict:
    """
    split the payload sequence into fragments
    makes room for the primers and the overhangs
    returns the dict of forward fragments
    """
    
    _, sequence = dfr.read_single_sequence_fasta(sequence_path)
        
    payload_length = fragment_length - OVERHANG_LENGTH

    ends_payload_length = fragment_length - len(bsaI_sequence) - 2*OVERHANG_LENGTH - PRIMER_LENGTH # length of payload in first and last fragments of each block
    
    dna_payload_per_block = 2 * ends_payload_length + (OLIGO_PER_BLOCK-2)* payload_length # total payload in a block
    
    # number of blocks in the sequence and rest
    n_blocks, r = divmod(len(sequence_wo_banwords), dna_payload_per_block)
    if r > 0 or n_blocks % OLIGO_PER_BLOCK != 0: # there shouldn't be rest, as last fragment is filled in binary conversion
        print("ordered fragmentation, no round number of blocks ("+str(n_blocks)+") : rest", str(r))
        exit(1)
        #n_blocks += 1
        #filling_sequence = dfr.complement(sequence[:payload_length-r]) # the filling should respects the dna constraints, but not be a part of the sequence
        #sequence += filling_sequence
    
    fragment_dict = {}
    
    index_sequence = 0 # index to browse the whole payload sequence
    
    for i in range(n_blocks*OLIGO_PER_BLOCK):
        if i % OLIGO_PER_BLOCK == 0 or i % OLIGO_PER_BLOCK == OLIGO_PER_BLOCK-1: # first or last fragment of a block
            fragment = sequence_wo_banwords[index_sequence:index_sequence+ends_payload_length]
            index_sequence += ends_payload_length
        else:
            fragment = sequence_wo_banwords[index_sequence:index_sequence+payload_length]
            index_sequence += payload_length
        
        fragment_dict["fragment_"+str(i)] = fragment 
    
    return fragment_dict


def add_overhangs(fragment_dict: dict) -> dict:
    """
    add overhangs to the fragments
    return a dict of forward and reverse fragments with the overhangs
    """
    
    overhang_list = list(dfr.read_fasta(currentdir+"/overhangs_eBlocks.fasta").values())
    overhang_fragment_dict = {}
    
    for i, (frag_name, frag_sequence) in enumerate(fragment_dict.items()):
        fragment_number = i % OLIGO_PER_BLOCK
        # overhangs are placed at the end of forward fragments
        if fragment_number == OLIGO_PER_BLOCK-1: # no overhang at the end of the last fragment of a block (not at this step)
            forward_total_fragment = frag_sequence
        else:
            forward_total_fragment = frag_sequence + overhang_list[fragment_number]
        overhang_fragment_dict[frag_name+"_Fw"] = forward_total_fragment
        
        # reverse complement of overhangs are placed at the end of reverse complement fragments
        if fragment_number == 0: # no overhang at the end of the reverse complement fragment of the first fragment of a block (not in at this step)
            reverse_total_fragment = dfr.reverse_complement(frag_sequence)
        else:
            reverse_total_fragment = dfr.reverse_complement(overhang_list[fragment_number-1]+frag_sequence)
        overhang_fragment_dict[frag_name+"_Rv"] = reverse_total_fragment
        
    return overhang_fragment_dict


def add_bsaI(fragment_dict: dict) -> (dict, str):
    """
    bsaI sites with an overhangs are placed to ends fragments
    returns dict of fragments with bsaI sites added to ends fragments,
    also returns the total sequence representing the theorical result of the forward assembly
    """
    
    overhangs_list = ["CTAT"] + list(dfr.read_fasta(currentdir+"/overhangs_eBlocks.fasta").values()) + ["AACA"]

    bsaI_dict = {}
    total_sequence = ""    
    
    ov_index = 0
    for i, (key, value) in enumerate(fragment_dict.items()):
        # first forward fragment of a block
        if i % (2*OLIGO_PER_BLOCK) == 0:
            bsaI_dict[key] = bsaI_sequence + overhangs_list[ov_index] + value
            total_sequence += overhangs_list[ov_index] + value
        # first reverse fragment of a block
        elif i % (2*OLIGO_PER_BLOCK) == 1:
            bsaI_dict[key] = value + dfr.reverse_complement(bsaI_sequence + overhangs_list[ov_index])
            ov_index += 1 # change the overhang for the next end fragment encountered
        # last forward fragment of a block
        elif i % (2*OLIGO_PER_BLOCK) == 2*OLIGO_PER_BLOCK-2:
            bsaI_dict[key] = value + overhangs_list[ov_index] + dfr.reverse_complement(bsaI_sequence)
            total_sequence += value # the linking overhang is with the first fragment of the next block
        # last reverse fragment of a block
        elif i % (2*OLIGO_PER_BLOCK) == 2*OLIGO_PER_BLOCK-1:
            bsaI_dict[key] = bsaI_sequence + dfr.reverse_complement(overhangs_list[ov_index]) + value
        # no bsaI sites for fragments insides the block
        else:
            bsaI_dict[key] = value
            total_sequence += value

    total_sequence += overhangs_list[ov_index]
    
    return bsaI_dict, total_sequence
    
    
def add_primers(bsaI_dict: dict, primers_list: list) -> dict:
    """
    add the primers to the first and last fragment of each blocks
    Block 1 :
        p1 + A1
             J1 + p2
    Block 2 :
        p3 + A2
             J2 + p4
    ...
    """
    primers_dict = {}
    primer_index = 0 # add the primers in the order of the list
    
    for i, (key, value) in enumerate(bsaI_dict.items()):
        # first forward fragment of a block
        if i % (2*OLIGO_PER_BLOCK) == 0:
            primers_dict[key] = primers_list[primer_index] + value
        # first reverse fragment of a block
        elif i % (2*OLIGO_PER_BLOCK) == 1:
            primers_dict[key] = value + dfr.reverse_complement(primers_list[primer_index])
            primer_index += 1
        # last forward fragment of a block
        elif i % (2*OLIGO_PER_BLOCK) == 2*OLIGO_PER_BLOCK-2:
            primers_dict[key] = value + dfr.reverse_complement(primers_list[primer_index])
        # last reverse fragment of a block
        elif i % (2*OLIGO_PER_BLOCK) == 2*OLIGO_PER_BLOCK-1:
            primers_dict[key] = primers_list[primer_index] + value
            primer_index += 1
        # no primers for fragments insides the block
        else:
            primers_dict[key] = value
    
    return primers_dict


def update_metadata_file(container_path: str, doc_index: str, encoding_data: dict) -> None:
    """
    add the encoding data informations of the document to the metadata file
    """
    metadata_manager_path = os.path.dirname(currentdir)+"/workflow_commands/metadata_manager.sh"
    for key, value in encoding_data.items():
        update_command = '. '+metadata_manager_path+' && add_doc_param '+container_path+' '+doc_index+' '+key+' '+str(value)
        subprocess.call('/bin/bash -c "$ADDPARAM"', shell=True, env={'ADDPARAM': update_command})



# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='divide a sequence into oligo fragments, add primers and overhang for a block assembly')
    parser.add_argument('-i', action='store', dest='sequence_path', required=True,
                        help='path to the complete payload sequence (.fasta)')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='path to the output fasta fragments')
    parser.add_argument('-l', action='store', dest='fragment_length', required=True,
                        type=int, help='length of the oligo fragments')
    parser.add_argument('--cont_doc', action='store', dest='container_doc_path', required=False,
                        help='path to the document in the container')
    
    
    # ---------- input list ---------------#
    arg = parser.parse_args()

    print("ordered fragment design...")    
    
    # split the sequence
    fragment_dict = sequence_fragmentation(arg.sequence_path, arg.fragment_length)
    # add the overhangs to the fragments
    overhang_dict = add_overhangs(fragment_dict) #TODO addition of overhang could create a bsaI site
    
    bsaI_dict, total_sequence = add_bsaI(overhang_dict)

    compatible_primers_list = pg.generate_compatible_primers(total_sequence)
    
    selected_primers_list = pg.select_primers(compatible_primers_list, len(fragment_dict)//OLIGO_PER_BLOCK)

    primers_block_dict = add_primers(bsaI_dict, selected_primers_list)
    
    dfr.save_dict_to_fasta(primers_block_dict, arg.output_path)
    

    # optional : update the metadata of the container
    if arg.container_doc_path is not None:
        encoding_data_dict = {"fragment_number":len(fragment_dict), "start_primer":selected_primers_list[0], "stop_primer":selected_primers_list[-1]}
        container_path, doc_index = os.path.split(os.path.normpath(arg.container_doc_path))
        update_metadata_file(container_path, doc_index, encoding_data_dict)

    print("\tcompleted !")

