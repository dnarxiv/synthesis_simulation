#!/usr/bin/python3

import os
import argparse
import random

import dna_file_reader as dfr


"""
simulations of eBlocks assembly into molecules
"""

# biological constraints for block assembly
buffer_size = 15
primer_size = 20
overhang_size = 4
bsaI_size = 7


def remove_buffer(eBlocks_list: list) -> list:
    """
    buffer at the beginning and end of each blocks
    """
    
    result_list = []
    for sequence in eBlocks_list:
        result_list.append(sequence[buffer_size:-buffer_size])
    
    return result_list


def digest_bsaI(eBlocks_list: list) -> list:
    """
    bsaI next to any overhangs, beginning and end of each blocks
    """
    
    result_list = []

    for sequence in eBlocks_list:
        result_list.append(sequence[bsaI_size:-bsaI_size])

    return result_list


def assemble_blocks(eBlocks_list: list) -> dict:
    """
    just a single perfect assembly of blocks
    """
        
    assembly_sequence = eBlocks_list[0]

    for sequence in eBlocks_list[1:]:
        if sequence[:overhang_size] == assembly_sequence[-overhang_size:]:
            assembly_sequence += sequence[overhang_size:]
        else:
            print("error assembly simulation, overhangs not compatibles",assembly_sequence[-overhang_size:],sequence[:overhang_size])
            exit(1)
    
    # remove overhangs before start primer and after stop primers in extremities blocks
    assembly_sequence = assembly_sequence[overhang_size:-overhang_size]
    
    # return a dict with 1 sequence
    return {"molecule_0" : assembly_sequence}


def assemble_blocks_simulation(eBlocks_list: list, nbr_mol: int) -> dict:
    """
    use the statistics of overhangs assemblies to simulation the assembly of blocks
    """
    overhang_assembly_stats_dict = dfr.read_fasta(os.path.abspath(os.path.dirname(__file__))+"/overhangs_eBlocks_probas.fasta")   
    
    assembly_dict = {}
    
    mol_index = 0
       
    for assembly_try in range(nbr_mol): # an assembly try can result in multiple molecules
        current_assembly = eBlocks_list[0] # start an assembly from the first block
        
        for block in eBlocks_list[1:]:
            last_overhang = current_assembly[-overhang_size:]
            
            if block[:overhang_size] != last_overhang:
                print("error assembly simulation, overhangs not compatibles", last_overhang, block[-overhang_size:])
                exit(1)
            
            overhang_link_proba = float(overhang_assembly_stats_dict[last_overhang])
            
            if random.random() <= overhang_link_proba: # chance to succeed overhang linking
                current_assembly += block[overhang_size:] # link the block to the current assembly
            else:
                assembly_dict["molecule_"+str(mol_index)] = current_assembly # the current assembly is not linked to the block, so its is completed
                mol_index += 1
                current_assembly = block[overhang_size:] # begin a new assembly starting from this block
                
        # add the assembly to the dict
        assembly_dict["molecule_"+str(mol_index)] = current_assembly
        mol_index += 1
    
    return assembly_dict


def test_assembly(input_path, output_path: str, nbr_mol: int) -> None:
    """
    print the assembly directly as a txt file of extracted blocks format
    for result analysis
    used to test simulation results
    """
    eBlocks_dict = dfr.read_fasta(input_path)
    blocks_list = list(eBlocks_dict.values())[::2] # get all odd elements of eBlocks dict (= forward fragments)

    # take a dict of only eBlocks, not tests simu for bsaI and buffer removal
    assembly_dict = assemble_blocks_simulation(blocks_list, nbr_mol)
    
    output = open(output_path, "w")
    
    for name, assembly in assembly_dict.items():
        list_frag = []
        assembly = assembly.replace("primer_____________", "") # remove the primer
        blocks_indexes = [i for i in range(len(assembly)) if assembly.startswith('block_', i)] # get all index of blocks in the assembly # supposedly inefficient in time but elegant
        
        for block_index in blocks_indexes:
            list_frag += [(assembly[block_index:block_index+8]+"_Fw", [block_index, block_index+8], '5p', '3p', 10)]
        
        framed_list_frag = [("First", [0, 0], "_", "_", 10)] + list_frag + [("Last", [len(assembly), len(assembly)], "_", "_", 10)] 
        
        output.write(name+"\n")
        output.write(assembly+"\n")
        output.write(str(framed_list_frag)+"\n")
        output.write("\n")
    output.close()



def eBlocks_assembly(input_path: str, output_dir_path: str, nbr_mol: int, assembly_number: int) -> None:
    """
    simulate an ordered assembly of eBlocks surrounded by the primers
    the resulting molecules are saved to a .fasta file to simulate the storing
    """
    
    eBlocks_dict = dfr.read_fasta(input_path)
    
    if len(eBlocks_dict) == 0:
        print("error : not sequences found at :",input_path)
        exit(1)
        
    blocks_list = list(eBlocks_dict.values())
    
    # simulate removal of buffers
    blocks_list = remove_buffer(blocks_list)
    # simulate bsaI digestion
    blocks_list = digest_bsaI(blocks_list)
    
    start_primer = blocks_list[0][overhang_size: overhang_size + primer_size] # get the start primer
    stop_primer = dfr.reverse_complement(blocks_list[-1][- overhang_size - primer_size: -overhang_size]) # the stop primer
    
    # name of the output file contains the 2 primers because the sequencing file is obtained from these primers
    output_file_path = os.path.join(output_dir_path, start_primer+"_"+stop_primer+"_"+str(assembly_number)+".fasta")
    
    # simulation assembly by overhangs, fatal error if overhangs are not compatibles for successive blocks
    #assembly_dict = assemble_blocks(blocks_list)
    assembly_dict = assemble_blocks_simulation(blocks_list, nbr_mol)
    # save the assemblies into file
    dfr.save_dict_to_fasta(assembly_dict, output_file_path)



def container_assembly(input_dir_path, output_dir_path, nbr_mol):
    
    assembly_number = 1 # number to add to the assembly name to distinguish assemblies with the same primers couple
    for filename in os.listdir(input_dir_path):
        file_path = os.path.join(input_dir_path, filename)
        
        if os.path.isdir(file_path):
            input_file = file_path+"/5_blocks_buffer.fasta"
            eBlocks_assembly(input_file, output_dir_path, nbr_mol, assembly_number)
            assembly_number += 1


# =================== main =======================#
if __name__ == '__main__':
    # --------- argument part -------------#
    parser = argparse.ArgumentParser(description='simulate molecule generation from eBlocks')
    parser.add_argument('-i', action='store', dest='input_dir_path', required=True,
                        help='the input fasta file')
    parser.add_argument('-o', action='store', dest='output_dir_path', required=True,
                        help='the output fasta file')
    parser.add_argument('-n', action='store', dest='nbr_mol', required=True,
                        type=int, help='number of molecules')

    # ---------- input list ---------------#
    arg = parser.parse_args()
    
    print("fragment assembly simulation...")
 
    container_assembly(arg.input_dir_path, arg.output_dir_path, arg.nbr_mol)
    #test_assembly(arg.input_dir_path, arg.output_dir_path, arg.nbr_mol)
    
    print("\tcompleted !")

